extends Label

var time = 0
var pm = false
var ser_day

var day: int
var month: int
var year: int

func _ready():
	Gbl.time = self

func _physics_process(_delta):
	time = Gbl.world.time
	ser_day = Gbl.world.day
	gregorian()
	text = str("Date: ", year, "-", month, "-", day, "\nTime (GMT): ", stepify(time, 0.1), "\nWarp (hr/sec): ", Gbl.warp)
	if year == 2101:
		Gbl.warp = 0
		Gbl.text_popup.show()
		Gbl.text_popup.get_node("Text").text = "Game Complete. Number of lives saved: %d lives." % Gbl.lives_saved

func gregorian():
	var f = int(ser_day + 1401 + int( int((4*ser_day+274277)/146097) * 3/4) - 38)
	var e: int = 4*f + 3
	var g = int(posmod(e, 1461)/4)
	var h = 5*g + 2
	day = int(posmod(h, 153) / 5) + 1
	month = posmod(int(h/153)+2, 12) + 1
	year = int(e/1461) + int((12+2-month) / 12) - 4716
