extends "res://UI_popup.gd"

var cost

func _on_Add_Cap_pressed():
	if Gbl.cash >= cost:
		Gbl.cash -= cost
		Gbl.selected_building.Installed_MW += $Input.value
		Gbl.UI.get_node("drill").play()
	else:
		Gbl.UI.get_node("error").play()

func _process(_delta):
	if Gbl.selected_building != null:
		var unit_cost = Gbl.cap_costs[Gbl.selected_building.type]
		cost = unit_cost * $Input.value
		$Calc.text = "x$%.2fM/MW = Cost: $%.2fM" % [unit_cost, cost]
		if cost > Gbl.cash:
			$Calc.add_color_override("font_color", Color.red)
		else:
			$Calc.add_color_override("font_color", Color.white)
	else:
		hide()
