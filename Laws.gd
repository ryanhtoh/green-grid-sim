extends "res://UI_popup.gd"

func _on_SCC_Slider_value_changed(value):
	Gbl.SCC = value
	$SCC.text = "$ %d / tonne CO2" % [value]
	Gbl.price_MWh = value*0.417 + 34.0
