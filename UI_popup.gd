extends Panel

var dragPoint = null

# Drag UI
func _on_gui_input(ev):
	if ev is InputEventMouseButton:
		if ev.button_index == BUTTON_LEFT:
			if ev.pressed:
				dragPoint = get_global_mouse_position() - get_position()
			else:
				dragPoint = null
	if ev is InputEventMouseMotion and dragPoint != null:
		set_position(get_global_mouse_position() - dragPoint)
		
func _on_Exit_pressed():
	Gbl.UI.get_node("click").play()
	hide()
	Gbl.mouse_on_UI = false

func _on_mouse_entered():
	Gbl.mouse_on_UI = true

func _on_mouse_exited():
	Gbl.mouse_on_UI = false
