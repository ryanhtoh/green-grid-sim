extends Node

var mouse_on_UI = true
var selected_building = null
var show_light = true
var to_load = false

var world
var UI
var popup
var rng
var plants
var backups
var cursor
var graph
var research
var text_popup
var solar_map
var wind_map

var mapsize

var time
var warp = 1.0 # In game hr / IRL second
var backup_cutoff = 1
var SCC = 0 # social cost of carbon
var price_MWh = 34.0
var us_power = 460000

var threshold_type = 0
var battery_storage = 0
var battery_stored = 0
var h2_storage = 0
var h2_stored = 0
var h2_power = 0

var sun_season = 0.0
var wind_season = 0.0

var cash = 10000.0
var researched = {
	"nuclear": false,
	"batteries": false
}
var energy_score = 0.0
var dmg_saved = 0.0
var lives_saved = 0.0

var cap_costs = {
	"solar": 1.1,
	"wind": 1.0,
	"csp": 5.0,
	"fission": 5.0,
	"battery": 0.1,
	"h2": 0.48
}
var var_costs = {
	"solar": 5.0,
	"wind": 6.0,
	"csp": 13.0,
	"fission": 29.0,
	"battery": 0.0,
	"h2": 0.0
}


func get_time(x_pos):
	var loc = x_pos / Gbl.mapsize
	var lon = 126 - loc*60
	var time = Gbl.time.time - lon/15
	if time < 0:
		time += 24
	return time

func get_daylight(time):
	var daylight
	if time < 6:
		daylight = 0
	elif time < 9:
		daylight = sin( PI*(time-6)/6 )
	elif time < 15:
		daylight = 1
	elif time < 18:
		daylight = cos( PI*(time-15)/6 )
	else:
		daylight = 0
	return daylight

func savegame():
	var save_game = File.new()
	save_game.open("user://savegame.save", File.WRITE)
	var gbl_dict = {
		"scene": "gbl",
		"cash": cash,
		"time": world.time,
		"day": world.day,
		"backup_cutoff": UI.pwr_slider * 100,
		"SCC": SCC,
		"price_MWh": price_MWh,
		"energy_score": energy_score,
		"researched": researched,
		"cap_costs": cap_costs,
		"var_costs": var_costs,
		"threshold_type": threshold_type,
		"battery_storage": battery_storage,
		"battery_stored": battery_stored,
		"h2_storage": h2_storage,
		"h2_stored": h2_stored,
		"h2_power": h2_power
	}
	save_game.store_line(to_json( gbl_dict ))
	
	for node in plants.get_children():
		var save_dict = {
			"scene": "plant",
			"x": node.position.x,
			"y": node.position.y,
			"type": node.type,
			"avgCF": node.avgCF,
			"Installed_MW": node.Installed_MW,
			"texture": node.texture
		}
		save_game.store_line(to_json( save_dict ))
	for node in backups.get_children():
		var save_dict = {
			"scene": "backup",
			"x": node.position.x,
			"y": node.position.y,
			"type": node.type,
			"Installed_MW": node.Installed_MW,
			"texture": node.texture
		}
		save_game.store_line(to_json( save_dict ))
	save_game.close()
	print("Game Saved")

func loadgame():
	var buttons = UI.get_node("toolbar/builds")
	var save_game = File.new()
	if not save_game.file_exists("user://savegame.save"):
		print("Save not found")
		return
	save_game.open("user://savegame.save", File.READ)
	for node in plants.get_children():
		node.queue_free()
	for node in backups.get_children():
		node.queue_free()
	while save_game.get_position() < save_game.get_len():
		var node_data = parse_json(save_game.get_line())
		match node_data["scene"]:
			"gbl":
				cash = node_data["cash"]
				world.time = node_data["time"]
				world.day = node_data["day"]
				backup_cutoff = node_data["backup_cutoff"]
				UI.get_node("Storage_Panel/Target_Pwr").value = backup_cutoff
				SCC = node_data["SCC"]
				price_MWh = node_data["price_MWh"]
				energy_score = node_data["energy_score"]
				researched = node_data["researched"]
				cap_costs = node_data["cap_costs"]
				var_costs = node_data["var_costs"]
				threshold_type = node_data["threshold_type"]
				battery_storage = node_data["battery_storage"]
				battery_stored = node_data["battery_stored"]
				h2_storage = node_data["h2_storage"]
				h2_stored = node_data["h2_stored"]
				h2_power = node_data["h2_power"]
			"plant":
				var plant = preload("res://Plant.tscn").instance()
				plants.add_child(plant)
				plant.avgCF = node_data["avgCF"]
				plant.type = node_data["type"]
				plant.Installed_MW = node_data["Installed_MW"]
				plant.position = Vector2(node_data["x"], node_data["y"])
				plant.set_texture(buttons.get_node(plant.type).texture_normal)
			"backup":
				var plant = preload("res://Plant.tscn").instance()
				backups.add_child(plant)
				plant.type = node_data["type"]
				plant.Installed_MW = node_data["Installed_MW"]
				plant.position = Vector2(node_data["x"], node_data["y"])
				plant.set_texture(buttons.get_node(plant.type).texture_normal)
	research.checks()
	save_game.close()

func numexp(num):
	var width = str(num).split(".", false, 1)[0].length()
	var exp_str = ""
	if width > 12:
		exp_str = "x10¹²"
		num /= 1000000000000
	elif width > 9:
		exp_str = "x10⁹"
		num /= 1000000000
	elif width > 6:
		exp_str = "x10⁶"
		num /= 1000000
	elif width > 3:
		exp_str = "x10³"
		num /= 1000
	else:
		exp_str = ""
	var string = "%.3f %s" % [num, exp_str]
	return string
