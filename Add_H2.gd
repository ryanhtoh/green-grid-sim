extends "res://UI_popup.gd"

var cost
var unit_cost = 0.48
var storage_cost
var storage_unit_cost = 0.004

func _process(_delta):
	cost = unit_cost * $Input.value
	$Calc.text = "x$%.2fM/MW = Cost: $%.2fM" % [unit_cost, cost]
	if cost > Gbl.cash:
		$Calc.add_color_override("font_color", Color.red)
	else:
		$Calc.add_color_override("font_color", Color.white)
		
	storage_cost = storage_unit_cost * $Input_Storage.value * 1000
	$Calc_Storage.text = "x$%.3fM/MWh = Cost: $%.2fM" % [storage_unit_cost, storage_cost]
	if storage_cost > Gbl.cash:
		$Calc_Storage.add_color_override("font_color", Color.red)
	else:
		$Calc_Storage.add_color_override("font_color", Color.white)

func _on_Add_Cap_pressed():
	if Gbl.cash >= cost:
		Gbl.cash -= cost
		Gbl.h2_power += $Input.value
		Gbl.UI.get_node("drill").play()
	else:
		Gbl.UI.get_node("error").play()

func _on_Add_Cap_Storage_pressed():
	if Gbl.cash >= storage_cost:
		Gbl.cash -= storage_cost
		Gbl.h2_storage += $Input_Storage.value* 1000
		Gbl.UI.get_node("drill").play()
	else:
		Gbl.UI.get_node("error").play()
