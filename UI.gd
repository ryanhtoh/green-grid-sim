extends CanvasLayer

var cost = 1.0
var build_buttons
var storage_buttons
var selected
var revenue = 0.0

var target_MW = 0.0
var target_MWh = 0.0

var graph_timer = 0.0
var tutorial_state = 0
var pwr_slider = 1.0

func _ready():
	Gbl.selected_building = null
	Gbl.UI = self
	Gbl.text_popup = $Text_Popup
	build_buttons = $toolbar/builds.get_children()
	for node in build_buttons:
		node.connect("pressed", self, "_build_press", [node])
		node.connect("mouse_entered", self, "_tooltip_on", [node, Vector2(0, -360)])
		node.connect("mouse_exited", self, "_tooltip_off", [node])
	storage_buttons = $Storage_Panel/builds.get_children()
	for node in storage_buttons:
		node.connect("mouse_entered", self, "_tooltip_on", [node, Vector2(-520, 0)])
		node.connect("mouse_exited", self, "_tooltip_off", [node])
	
	$Text_Popup.connect("mouse_entered", self, "mouse_on_UI")
	$Text_Popup.connect("mouse_exited", self, "mouse_off_UI")
	$Storage_Panel.connect("mouse_entered", self, "mouse_on_UI")
	$Storage_Panel.connect("mouse_exited", self, "mouse_off_UI")
	$Menu.connect("mouse_entered", self, "mouse_on_UI")
	$Menu.connect("mouse_exited", self, "mouse_off_UI")
	$Storage_Panel/SuggestType.add_item("Week")
	$Storage_Panel/SuggestType.add_item("Year")

func _process(_delta):
	$toolbar/season.text = "Seasonal Sun Strength: %.1f%% \nSeasonal Wind Strength: %.1f%%" % [Gbl.sun_season*100, Gbl.wind_season*100]

func _physics_process(delta):
	if Gbl.warp > 0:
		var mws = {
			"sum": 0,
			"pv": 0,
			"csp": 0,
			"wind": 0,
			"fission": 0,
			"battery": 0
		}
		var var_costs = 0
		target_MW = 0
		target_MWh = 0
		var target_h2_power = 0
		
		for plant in Gbl.plants.get_children():
			target_h2_power += plant.Installed_MW
			target_MW += plant.target_MW
			target_MWh += plant.target_MWh
			mws["sum"] += plant.Current_MW
			match plant.type:
				"solar": mws["pv"] += plant.Current_MW
				"wind": mws["wind"] += plant.Current_MW
				"csp": mws["csp"] += plant.Current_MW
				"fission": mws["fission"] += plant.Current_MW
			var_costs += plant.Current_MW * Gbl.var_costs[plant.type]
		
		Gbl.backup_cutoff = pwr_slider * target_MW
		var actual_gen = mws["sum"]
		
		var deficit = Gbl.backup_cutoff - mws["sum"]
		var transfer = clamp(deficit*delta*Gbl.warp, Gbl.battery_stored-Gbl.battery_storage, Gbl.battery_stored) # discharge
		mws["sum"] += transfer/(delta*Gbl.warp)
		mws["battery"] += transfer/(delta*Gbl.warp)
		Gbl.battery_stored -= transfer
		var bat_str = "Batteries: \nStored: %s MWh \nStorage: %s MWh \nSuggested Storage: >%s MWh \n\n" % [Gbl.numexp(Gbl.battery_stored), Gbl.numexp(Gbl.battery_storage), Gbl.numexp(target_MWh)]
		
		deficit = clamp(Gbl.backup_cutoff - mws["sum"], -Gbl.h2_power, Gbl.h2_power)
		transfer = clamp(deficit*delta*Gbl.warp, Gbl.h2_stored-Gbl.h2_storage, Gbl.h2_stored) # discharge
		var eff_mult = 1
		if transfer < 0: eff_mult = 0.37 # If charging, lower efficiency multiplier
		mws["sum"] += transfer/(delta*Gbl.warp)
		mws["battery"] += transfer/(delta*Gbl.warp)
		Gbl.h2_stored -= eff_mult*transfer
		var h2_str = "Hydrogen: \nStored: %s MWh \nStorage: %s MWh \nCurrent Power: %d MW \nMax Power: %d MW \nSuggested Max Power: <%d MW \n\n" % [Gbl.numexp(Gbl.h2_stored), Gbl.numexp(Gbl.h2_storage), transfer, Gbl.h2_power, target_h2_power]
		$Storage_Panel/Text.text = bat_str + h2_str
		$Storage_Panel/Target_Pwr_Lbl.text = "Suggested Threshold: %d MW \nSet Threshold: %d MW" % [target_MW, Gbl.backup_cutoff]
		
		graph_timer += delta*Gbl.warp
		if graph_timer > 0.5:
			Gbl.graph.add_point(mws, graph_timer)
			graph_timer = 0
		var min_mw = Gbl.graph.min_mw
		Gbl.energy_score += min_mw * delta*Gbl.warp
		Gbl.dmg_saved = Gbl.energy_score * 0.46 * 51 / 1000000
		Gbl.lives_saved = Gbl.energy_score / 2396
		revenue = (min_mw * Gbl.price_MWh) / 1000000
		if actual_gen > 0:
			var_costs *= min((min_mw / actual_gen), 1) / 1000000
		var pwr_str = "MW Met: %.0f MW \nShare of US Market: %.1f%% \nPrice: $%.1f/MWh" \
		+ "\nWeekly Revenue: $%.2f M \nWeekly Costs: $%.2f M \nWeekly Profit: $%.2f M"
		$toolbar/pwr.text = pwr_str % [min_mw, 100*min_mw/Gbl.us_power, Gbl.price_MWh, revenue*7*24, var_costs*7*24, (revenue-var_costs)*7*24]
		Gbl.cash += (revenue - var_costs) * delta*Gbl.warp
	$toolbar/cash.text = "Cash: $%.3f M \n\nEnergy Score: %d TWh \nDamage Averted: $%dM \nLives Saved: %d" % [Gbl.cash, Gbl.energy_score/1000000, Gbl.dmg_saved, Gbl.lives_saved]

func _input(event):
	if event.is_action_pressed("ui_cancel"):
		for node in build_buttons:
			node.pressed = false
		$toolbar/builds/select.pressed = true
		selected = null
		Gbl.cursor.get_node("Sprite").texture = null

func _build_press(button):
	$click.play()
	if button.pressed:
		for node in build_buttons:
			node.pressed = false
		button.pressed = true
		if button != $toolbar/builds/select:
			selected = button
			Gbl.cursor.get_node("Sprite").texture = selected.texture_normal
			if button.name == "solar" or button.name == "csp":
				Gbl.solar_map.show()
				Gbl.wind_map.hide()
				$toolbar/overlays/sun.pressed = true
				$toolbar/overlays/wind.pressed = false
			elif button.name == "wind":
				Gbl.solar_map.hide()
				Gbl.wind_map.show()
				$toolbar/overlays/sun.pressed = false
				$toolbar/overlays/wind.pressed = true
		else:
			selected = null
			Gbl.cursor.get_node("Sprite").texture = null
	else:
		selected = null
		Gbl.cursor.get_node("Sprite").texture = null

func _on_Popup_mouse_entered():
	Gbl.mouse_on_UI = true
func _on_toolbar_mouse_entered():
	Gbl.mouse_on_UI = true

func _on_Popup_mouse_exited():
	Gbl.mouse_on_UI = false
func _on_toolbar_mouse_exited():
	Gbl.mouse_on_UI = false

func _on_Popup_Exit_pressed():
	$click.play()
	$Popup.hide()
	Gbl.mouse_on_UI = false


func _on_Add_pressed():
	$click.play()
	$Add_Cap.show()

func _on_slow_pressed():
	$click2.play()
	if Gbl.warp != 0:
		if Gbl.warp >= 50:
			Gbl.warp /= 2
		elif Gbl.warp > 5:
			Gbl.warp = 5
		elif Gbl.warp > 1:
			Gbl.warp = 1
	else:
		Gbl.warp = 1
	Engine.iterations_per_second = max(int(Gbl.warp/2), 60)

func _on_fast_pressed():
	$click2.play()
	if Gbl.warp != 0:
		if Gbl.warp < 5:
			Gbl.warp = 5
		elif Gbl.warp < 25:
			Gbl.warp = 25
		elif Gbl.warp < 3200:
			Gbl.warp *= 2
	else:
		Gbl.warp = 1
	Engine.iterations_per_second = max(int(Gbl.warp/2), 60)

func _on_pause_pressed():
	$click2.play()
	Gbl.warp = 0
	Engine.iterations_per_second = 60

func _on_graph_button_pressed():
	$click.play()
	Gbl.graph.show()

func _on_Limit_value_changed(value):
	Gbl.selected_building.limit = value

func _on_Text_Exit_pressed():
	$Text_Popup.hide()
	$HintBoxes.hide()
	$click.play()
	Gbl.mouse_on_UI = false

func mouse_on_UI():
	Gbl.mouse_on_UI = true
	
func mouse_off_UI():
	Gbl.mouse_on_UI = false


func _on_Demolish_pressed():
	if Gbl.selected_building != null:
		$break.play()
		Gbl.selected_building.queue_free()
		Gbl.selected_building = null


func _on_daylight_toggled(button_pressed):
	Gbl.show_light = button_pressed


func _on_sun_toggled(button_pressed):
	if button_pressed:
		Gbl.solar_map.show()
		Gbl.wind_map.hide()
		$toolbar/overlays/sun.pressed = true
		$toolbar/overlays/wind.pressed = false
	else:
		pass

func _on_wind_toggled(button_pressed):
	if button_pressed:
		Gbl.solar_map.hide()
		Gbl.wind_map.show()
		$toolbar/overlays/sun.pressed = false
		$toolbar/overlays/wind.pressed = true
	else:
		pass

func _on_Target_Pwr_value_changed(value):
	pwr_slider = value/100

func _tooltip_on(button, offset):
	$Tooltip.show()
	$Tooltip.rect_global_position = button.rect_global_position + offset
	if button.name == "csp":
		$Tooltip/Image.texture = preload("res://Images/CSP.jpg")
		var topstr = "Concentrated Solar Thermal: \nPrice: $%.2fM / MW \nOperating Cost: $%.2fM / MWh"
		$Tooltip/TopTxt.text = topstr % [Gbl.cap_costs[button.name], Gbl.var_costs[button.name]]
		$Tooltip/BotTxt.text = "Concentrated Solar Thermal Systems generate power by focusing sunlight on a receiver, " \
		+ "generating heat (up to 565 °C or 1,049 °F). They can generate power at night by storing heat in molten salts. " \
		+ "While providing stable power, they have expensive upfront and operating costs, " \
		+ "and are more sensitive to location than solar PV."
	if button.name == "solar":
		$Tooltip/Image.texture = preload("res://Images/PV.jpg")
		var topstr = "Solar PV: \nPrice: $%.2fM / MW \nOperating Cost: $%.2fM / MWh"
		$Tooltip/TopTxt.text = topstr % [Gbl.cap_costs[button.name], Gbl.var_costs[button.name]]
		$Tooltip/BotTxt.text = "Solar Photovoltaics Systems generate power from light by the photovoltaic effect. " \
		+ "They are cheap to build and operate, but don't produce power at night, and are sensitive to location. " \
		+ "Use backup sources to provide stable power. Batteries are good for the daily power swings while Hydrogen is good for seasonal power swings."
	if button.name == "fission":
		$Tooltip/Image.texture = preload("res://Images/fission.jpg")
		var topstr = "Nuclear Fission Plant: \nPrice: $%.2fM / MW \nOperating Cost: $%.2fM / MWh"
		$Tooltip/TopTxt.text = topstr % [Gbl.cap_costs[button.name], Gbl.var_costs[button.name]]
		$Tooltip/BotTxt.text = "A Nuclear Reactor uses controlled nuclear fission to generate stable baseload power. " \
		+ "Their power does not depend on location. However, they have expensive upfront and operating costs."
	if button.name == "wind":
		$Tooltip/Image.texture = preload("res://Images/wind.jpg")
		var topstr = "Wind Turbine: \nPrice: $%.2fM / MW \nOperating Cost: $%.2fM / MWh"
		$Tooltip/TopTxt.text = topstr % [Gbl.cap_costs[button.name], Gbl.var_costs[button.name]]
		$Tooltip/BotTxt.text = "Wind Turbines convert kinetic energy from the wind into electrical energy. " \
		+ "They are cheap to build and operate, but are location dependent and the power generated is unreliable. " \
		+ "Use multiple turbines and/or backup sources to provide stable power. Batteries are good for daily power swings while Hydrogen is good for longer term swings."
	if button.name == "battery":
		$Tooltip/Image.texture = preload("res://Images/battery.jpg")
		var topstr = "Battery Storage: \nPrice: $%.2fM / MWh \nOperating Cost: $%.2fM / MWh"
		$Tooltip/TopTxt.text = topstr % [Gbl.cap_costs[button.name], Gbl.var_costs[button.name]]
		$Tooltip/BotTxt.text = "Batteries store energy during excess generation and discharge it when it is needed. They are cheap, powerful, and efficient, but they have expensive storage capacity. As such, they are best used for daily/short-term power swings (like from solar) and not for long term ones."
	if button.name == "h2":
		$Tooltip/Image.texture = preload("res://Images/h2.jpg")
		var topstr = "Hydrogen Storage: \nPower Price: $%.2fM / MW \nStorage Price: $%.3fM / MWh"
		$Tooltip/TopTxt.text = topstr % [Gbl.cap_costs[button.name], $Add_H2.storage_unit_cost]
		$Tooltip/BotTxt.text = "Hydrogen electrolyzers produce hydrogen from power during excess generation, and fuel cells convert it back into power during excess demand. Round trip efficiency is only 37%, but storage costs are small. As such, they are best used for long term power swings associated with the seasons and wind power."
	if button.name == "select":
		$Tooltip.hide()

func _tooltip_off(_button):
	$Tooltip.hide()


func _on_Next_pressed():
	$click.play()
	match tutorial_state:
		-1:
			$Text_Popup/Text.text = "The United States has implemented a HVDC (High Voltage Direct Current) system that connects the three regional grids together, allowing intermittent renewables across the country to be averaged together, improving reliability, cost, and efficiency for relatively low cost. \n\nYour job is to build low carbon energy sources to power the US."
		0: 
			$Text_Popup/Text.text = "You are paid based on the LEAST power generated in the last week, any excess power generated above the baseload will be curtailed (wasted). \n\nAs such, use energy storage, geographic diversity, and/or stable generation from sources like nuclear or solar thermal for reliability. This also means your first week of generating power will give no income."
		1:
			$Text_Popup/Text.text = "The game will end after the year 2100. Your final score is deaths prevented from global warming. \n\nNote: All numbers and costs (except Research costs) in this game are taken from real values from industry and scientific papers. See citations for more details."
		2:
			$HintBoxes/H1.show()
			$Text_Popup/Text.text = "This is the Build Menu. Build power plants and storage systems here. Hover over an icon to see details. \n\nClick on the map to build. Press the hand icon or Esc to cancel building placement."
		3:
			$Text_Popup/Text.text = "Once a building is placed, you must add capacity using the Add Capacity popup to create power. You can bring this popup back anytime by pressing the outlined Add Capacity button in the top right. \n\nWhile not building anything, you can select different buildings by clicking on them."
			$HintBoxes/H1.hide()
			$HintBoxes/H2.show()
		4:
			$Text_Popup/Text.text = "The right of the screen controls energy storage. Add batteries for daily power swings, and hydrogen for longer swings. Note: Hydrogen is only 37% efficient. You can control the threshold at which the storages charge and discharge by setting the Power Threshold field outlined below the stats. Batteries will be charged and discharged before hydrogen for efficiency."
			$HintBoxes/H2.hide()
			$HintBoxes/H3.show()
			$HintBoxes/H3B.show()
		5:
			$Text_Popup/Text.text = "You can see and control the time here. Uses the Pause, Slow, and Fast Forward buttons here to warp through time."
			$HintBoxes/H3.hide()
			$HintBoxes/H3B.hide()
			$HintBoxes/H4.show()
		6:
			$Text_Popup/Text.text = "You can show or hide overlays here. It is suggested to toggle off the Daylight overlay during fast time warps to hide the strobing effect. \n\nThe Graph overlay shows power generation through the week. The sun and wind overlays show maps of solar and wind resources, which affect the power generated from related plants."
			$HintBoxes/H4.hide()
			$HintBoxes/H5.show()
		7:
			$Text_Popup/Text.text = "Use the mouse wheel to zoom in and out. Hold down the mouse wheel to drag the map. \n\nTime to save lives and make some money!"
			$HintBoxes/H5.hide()
			$Text_Popup/Next.text = "Close"
		8:
			_on_Text_Exit_pressed()
	tutorial_state += 1


func _on_credits_pressed():
	$Credits.show()
	Gbl.UI.get_node("click").play()


func _on_tutorial_pressed():
	tutorial_state = -1
	$Text_Popup.show()
	$HintBoxes.show()
	for node in $HintBoxes.get_children():
		node.hide()
	_on_Next_pressed()
	Gbl.UI.get_node("click").play()


func _on_Difficulty_pressed():
	$Laws.show()
	Gbl.UI.get_node("click").play()


func _on_Menu_pressed():
	Gbl.UI.get_node("click").play()
	get_tree().reload_current_scene()
	get_tree().change_scene("res://Main_Menu.tscn")

func _on_Save_pressed():
	Gbl.UI.get_node("click").play()
	Gbl.savegame()

func _on_Load_pressed():
	Gbl.UI.get_node("click").play()
	Gbl.loadgame()

func _on_Quit_pressed():
	get_tree().quit()

func _on_Research_pressed():
	$click.play()
	$Research.show()

func _on_battery_pressed():
	$click.play()
	$Add_Battery.show()

func _on_h2_pressed():
	$click.play()
	$Add_H2.show()


func _on_SuggestType_item_selected(index):
	Gbl.threshold_type = index
