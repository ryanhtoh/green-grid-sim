extends Panel

var cost_batteries = 11000
var cost_nuclear = 35000

func _ready():
	Gbl.research = self
	checks()

func _on_Button_pressed():
	$click.play()
	hide()

func _get_Nuclear():
	if Gbl.cash > cost_nuclear and Gbl.researched["nuclear"] == false:
		Gbl.cash -= cost_nuclear
		Gbl.researched["nuclear"] = true
		Gbl.var_costs["fission"] /= 2
		Gbl.cap_costs["fission"] /= 2
		$Nuclear/check.show()
		$chime.play()
	else:
		$error.play()


func _get_Batteries():
	if Gbl.cash > cost_batteries and Gbl.researched["batteries"] == false:
		Gbl.cash -= cost_batteries
		Gbl.researched["batteries"] = true
		Gbl.cap_costs["battery"] *= 0.4
		$Batteries/check.show()
		$chime.play()
	else:
		$error.play()

func checks():
	if Gbl.researched["nuclear"]:
		$Nuclear/check.show()
	if Gbl.researched["batteries"]:
		$Batteries/check.show()
