extends Sprite

var img = Image.new()
var MAX = 100
var MIN = 58

# Called when the node enters the scene tree for the first time.
func _ready():
	var stream_texture = load("res://Data/solar3.PNG")
	img = stream_texture.get_data()
	img.lock()
	Gbl.solar_map = self


func get_watts(loc):
	if loc.x <= 0 or loc.y <= 0 or loc.x >= img.get_size().x or loc.y >= img.get_size().y:
		return 0
	var cfac = (1-img.get_pixelv(loc).h)
	if cfac == 1 or get_color(loc).v < 0.2:
		cfac = 0
	else:
		cfac -= 0.3
		if cfac <= 0:
			cfac += 1
		cfac = MIN + (MAX-MIN)*cfac
	return cfac

func get_color(loc):
	return img.get_pixelv(loc)
