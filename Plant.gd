extends Node2D

var Installed_MW = 0
var Current_MW = 0

var CF = 0
var seasonal_CF = 0
var wind_CF = 0
var daylight = 0
var avgCF = 1.0

var target_MW = 0
var target_MWh = 0

var texture
var type = null
var string = ""
var time = 0
var last_hr = 0

func _ready():
	select_self()

func _physics_process(_delta):
	time = Gbl.get_time(position.x)
	daylight = Gbl.get_daylight(time)
	target_MWh = 0
	
	if type == "solar":
		seasonal_CF = Gbl.sun_season
		CF = seasonal_CF * daylight * avgCF/100
		if Gbl.threshold_type == 0:
			target_MW = 0.409 * Installed_MW * seasonal_CF * avgCF/100
		else:
			target_MW = 0.409 * Installed_MW * 0.75 * avgCF/100
		target_MWh = 0.409 * Installed_MW * (avgCF/100) * 14.2
	elif type == "wind":
		seasonal_CF = Gbl.wind_season
		if Gbl.world.time < last_hr:
			last_hr = 0
		while Gbl.world.time >= last_hr + 1:
			last_hr += 1
			if last_hr > 24:
				last_hr -= 24
			var rand = Gbl.rng.randfn(0.0, 0.06)
			wind_CF = clamp(wind_CF + rand, 0, 1.0)
		CF = seasonal_CF * wind_CF * avgCF/100
		if Gbl.threshold_type == 0:
			target_MW = 0.5 * 0.66 * Installed_MW * seasonal_CF * avgCF/100
		else:
			target_MW = 0.5 * 0.66 * Installed_MW * 0.85 * avgCF/100
	elif type == "csp":
		seasonal_CF = Gbl.sun_season
		CF = seasonal_CF * (0.375*avgCF/100) * (avgCF/100)
		if Gbl.threshold_type == 0:
			target_MW = Installed_MW * CF
		else:
			target_MW = Installed_MW * 0.75 * (0.375*avgCF/100) * (avgCF/100)
	elif type == "fission":
		CF = 1.0
		target_MW = Installed_MW
	$CF.value = CF*100
	Current_MW = Installed_MW * CF
	if Gbl.selected_building == self:
		set_UI_text()

func set_texture(img):
	texture = img
	$Sprite.texture = img

func set_UI_text():
	concat("Installed Power: %.0f MW" % Installed_MW)
	concat("Current Power: %.0f MW" % Current_MW)
	concat("Local Time: %.1f" % time)
	concat("\nOverall Strength (Capacity Factor): %.1f%%" % [CF*100])
	if type != "fission":
		concat("Location Strength: %.1f%%" % avgCF)
		concat("Seasonal Strength: %.1f%%" % [seasonal_CF*100])
	if type == "solar":
		concat("Sun Strength: %.1f%%" % [daylight*100])
	elif type == "wind":
		concat("Wind Strength: %.1f%%" % [wind_CF*100])
		#concat("Wind hr %.2f" % last_hr)
	elif type == "csp":
		concat("CSP Strength: %.1f%%" % [0.375*avgCF])
	elif type == "fission":
		pass
	concat("\nMarginal Cost: $%.1f / MWh" % [Gbl.var_costs[type]])
#	concat("\nDegradation Rate: %.1f%% / yr" % 0.51)
#	concat("\nCapacity Limit: %d%%" % limit)
	Gbl.popup.get_node("Text").text = string
	string = ""

func concat(to_add):
	string = string + to_add + "\n"


func _on_Area2D_input_event(_viewport, event, _shape_idx):
	if event is InputEventMouseButton and event.button_index == 1 and Gbl.UI.selected == null:
		select_self()

func select_self():
	Gbl.UI.get_node("Popup").show()
	if Gbl.selected_building != null:
		Gbl.selected_building.get_node("Frame").hide()
	$Frame.show()
	Gbl.selected_building = self
	if type == "battery":
		Gbl.UI.get_node("Add_Cap/Input").suffix = "MWh"
	else:
		Gbl.UI.get_node("Add_Cap/Input").suffix = "MW"
