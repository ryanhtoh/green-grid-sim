extends "res://UI_popup.gd"

var WIDTH = 1100

var maxval = 1.0
var minval = 0.0
var min_mw = 0.0

var MARGIN = 1.05
var sum_pts = PoolVector2Array()
var pv_pts = PoolVector2Array()
var csp_pts = PoolVector2Array()
var wind_pts = PoolVector2Array()
var fission_pts = PoolVector2Array()
var battery_pts = PoolVector2Array()
var show_sum = true
var show_pv = true
var show_csp = true
var show_wind = true
var show_fission = true
var show_battery = true
var minmax_sum = Vector2.ZERO
var minmax_pv = Vector2.ZERO
var minmax_csp = Vector2.ZERO
var minmax_wind = Vector2.ZERO
var minmax_fission = Vector2.ZERO
var minmax_battery = Vector2.ZERO

func _ready():
	Gbl.graph = self

func add_point(vals, delta):
	maxval = 1
	minval = 0
	min_mw = Gbl.us_power 
	sum_pts = add_subpoint(vals["sum"], sum_pts, delta, true)
	pv_pts = add_subpoint(vals["pv"], pv_pts, delta, false)
	csp_pts = add_subpoint(vals["csp"], csp_pts, delta, false)
	wind_pts = add_subpoint(vals["wind"], wind_pts, delta, false)
	fission_pts = add_subpoint(vals["fission"], fission_pts, delta, false)
	battery_pts = add_subpoint(vals["battery"], battery_pts, delta, false)
	$Max.text = "%.0f MW" % maxval
	$Min.text = "%.0f MW" % minval
	$Val.text = "%.0f MW" % vals["sum"]
	update()

func _draw():
	if show_sum: draw_graph(sum_pts, Color.white, 2)
	if show_pv: draw_graph(pv_pts, Color.yellow, 1)
	if show_csp: draw_graph(csp_pts, Color.red, 1)
	if show_wind: draw_graph(wind_pts, Color.aqua, 1)
	if show_fission: draw_graph(fission_pts, Color.green, 1)
	if show_battery: draw_graph(battery_pts, Color.purple, 1)

func add_subpoint(val, points, delta, is_master):
	for point_idx in points.size():
		var point = points[point_idx]
		point.x -= delta * (5.952380952381)
		points.set(point_idx, point)
		if is_master:
			if point.y < min_mw:
				min_mw = point.y
		if point.y*MARGIN > maxval:
			maxval = point.y*MARGIN
		elif point.y < minval:
			minval = point.y
	points.append(Vector2(WIDTH, val))
	if is_master:
		if val < min_mw:
			min_mw = val
	if val*MARGIN > maxval:
		maxval = val*MARGIN
	elif val < minval:
		minval = val
	while points[0].x < 100:
		points.remove(0)
	return points


# draws an array's graph
func draw_graph(points, color, width):
	if points.size() > 0:
		var point1 = points[0]
		for point2 in points:
			var point1_scaled = point1
			point1_scaled.y = 275-250*(point1_scaled.y - minval)/(maxval-minval)
			var point2_scaled = point2
			point2_scaled.y = 275-250*(point2_scaled.y - minval)/(maxval-minval)
			draw_line(point1_scaled, point2_scaled, color, width)
			point1 = point2

func _on_Sum_toggled(button_pressed):
	show_sum = button_pressed
	update()
func _on_PV_toggled(button_pressed):
	show_pv = button_pressed
	update()
func _on_CSP_toggled(button_pressed):
	show_csp = button_pressed
	update()
func _on_Wind_toggled(button_pressed):
	show_wind = button_pressed
	update()
func _on_Fission_toggled(button_pressed):
	show_fission = button_pressed
	update()
func _on_Storage_toggled(button_pressed):
	show_battery = button_pressed
	update()
