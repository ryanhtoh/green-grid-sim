extends "res://UI_popup.gd"

var cost
var unit_cost = 0.1

func _on_Add_Cap_pressed():
	if Gbl.cash >= cost:
		Gbl.cash -= cost
		Gbl.battery_storage += $Input.value * 1000
		Gbl.UI.get_node("drill").play()
	else:
		Gbl.UI.get_node("error").play()

func _process(_delta):
	unit_cost = Gbl.cap_costs["battery"]
	cost = unit_cost * $Input.value * 1000
	$Calc.text = "x$%.2fM/MWh = Cost: $%.2fM" % [unit_cost, cost]
	if cost > Gbl.cash:
		$Calc.add_color_override("font_color", Color.red)
	else:
		$Calc.add_color_override("font_color", Color.white)
