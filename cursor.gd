extends Node2D

var pix = Vector2(20,10)
var TIMES_IN_DY = 30
var can_build = false

func _ready():
	Gbl.cursor = self

func _unhandled_input(event):
	if event is InputEventMouseButton and event.button_index == 1 and event.pressed == false:
		if Gbl.UI.selected != null and can_build:
			build(get_global_mouse_position())

func _process(_delta):
	var pos = get_global_mouse_position()
	position = pos
	
	if $Area2D.get_overlapping_areas() == [] and not Gbl.mouse_on_UI:
		can_build = true
		$Sprite.modulate = Color(0, 1, 0, 0.8)
	else:
		can_build = false
		$Sprite.modulate = Color(1, 0, 0, 0.8)
	
	if not Gbl.mouse_on_UI:
		var solar_cf = Gbl.solar_map.get_watts(pos)
		var wind_cf = Gbl.wind_map.get_watts(pos)
		$label.text = "Sun Strength: %.1f%%, Wind Strength: %.1f%%" % [solar_cf, wind_cf]

func build(loc):
	Gbl.UI.get_node("screw").play()
	var node = preload("res://Plant.tscn").instance()
	node.type = Gbl.UI.selected.name
	if node.type == "battery":
		Gbl.backups.add_child(node)
	else:
		Gbl.plants.add_child(node)
	node.set_texture($Sprite.texture)
	node.position = loc
	Gbl.UI.get_node("Add_Cap").show()
	
	if node.type == "wind":
		node.avgCF = Gbl.wind_map.get_watts(position)
	elif node.type == "solar":
		node.avgCF = Gbl.solar_map.get_watts(position)
	elif node.type == "csp":
		node.avgCF = Gbl.solar_map.get_watts(position)
