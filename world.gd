extends Node2D

var test1 = 0.0
var test2 = 0.0
var test3 = 0.0

var time = 0
var day = 2459571

var pix = Vector2(25.13333333333, 904)
var TIMES_IN_DY = 24

func _ready():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	Gbl.rng = rng
	Gbl.world = self
	Gbl.popup = $UI/Popup
	Gbl.plants = $plants
	Gbl.backups = $backups
	Gbl.mapsize = $map/solar.get_rect().size.x
	if Gbl.to_load:
		Gbl.loadgame()
		Gbl.to_load = false

#func _process(_delta):
#	if Input.is_action_pressed("1"):
#		Gbl.solar_map.show()
#		Gbl.wind_map.hide()
#	if Input.is_action_pressed("2"):
#		Gbl.solar_map.hide()
#		Gbl.wind_map.show()

func _physics_process(delta):
	time += delta*Gbl.warp
	if time >= TIMES_IN_DY:
		time -= TIMES_IN_DY
		day += 1
		test2 = OS.get_ticks_msec()
		test3 = (test2 - test1)
		print(test3)
		test1 = test2
	var year_frac = (Gbl.time.month-1 + Gbl.time.day/30.4)/12
	Gbl.sun_season = sin(2*PI*year_frac-PI*0.3333)*0.25 + 0.75
	Gbl.wind_season = sin(2*PI*year_frac+PI*0.4456)*0.15 + 0.85
	
	update()

func _draw():
	if Gbl.show_light:
		draw_sunlight()

func draw_sunlight():
	var x_steps = 60
	for x in range(x_steps):
		var local_time = Gbl.get_time(x*pix.x)
		var daylight = Gbl.get_daylight(local_time)
		var color = Color(daylight, daylight, 0, 0.5)
		draw_rect(Rect2(Vector2(x*pix.x, 0), pix), color)
