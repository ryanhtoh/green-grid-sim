extends Control


func _on_New_pressed():
	$click.play()
	get_tree().change_scene("res://world.tscn")


func _on_Load_pressed():
	$click.play()
	get_tree().change_scene("res://world.tscn")
	Gbl.to_load = true


func _on_Quit_pressed():
	get_tree().quit()
